FROM php:7.2-apache

# Image Information
LABEL maintener="AD Soft" \
      author="Wilson Silva dos Anjos" \
      mail="maxwilson465@gmail.com" \
      create="2020/08/01" \
      version="1.0.0" \
      description="Especify the image Docker of app" \
      license="gpl-v3"

COPY css/ /var/www/html/css/
COPY index.php /var/www/html/