# contact-me

Atividade final DevOps

# 1 - Git Clone no projeto

No console der **[ git clone https://gitlab.com/maxwilson/contact-me ]**

# 2 - Acessando diretorio

No console [ cd /contact-me ]

# 3 - Build

Realize build da aplicação **[ docker build -t wlsanjos/contactphp:1.0.0 . ]**

# 4 - Iniciando aplicação

**obs** Precisa liberar porta 80 para funcionar.
No console **[ docker run -d -p 80:80 --name unipe wlsanjos/contactphp:1.0.0 ]**

# 5 - Acessando site
Acesse ip da sua maquina virtual/cloud

# Imagem do docker
  Fazendo pelo docker Hub

  # 1 - Realiza pull do repositorio
  no console **[ docker pull wlsanjos/contactphp:1.0.0 ]**

  # 2 - Rodando imagem
  no console **[ docker run -d -p 80:80 --name unipe wlsanjos/contactphp:1.0.0 ]**
  
  # 3 - Acessando site
  Acesse ip da sua maquina virtual/cloud