<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!------ HEAD ---------->
<link href="./css/style.css" rel="stylesheet" id="bootstrap-css">
<div class="container contact-form">
	<div class="contact-image">
		<img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
	</div>
	<form method="post">
		<h3>Contato me - <?php echo date("d/m/Y");?></h3>
	   <div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<input type="text" name="txtName" class="form-control" placeholder="Seu nome *" value="" />
				</div>
				<div class="form-group">
					<input type="text" name="txtEmail" class="form-control" placeholder="Seu e-mail *" value="" />
				</div>
	
				<div class="form-group">
					<input type="submit" name="btnSubmit" class="btnContact" value="Enviar" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<textarea name="txtMsg" class="form-control" placeholder="Descrição *" style="width: 100%; height: 150px;"></textarea>
				</div>
			</div>
		</div>
    </form>
</div>